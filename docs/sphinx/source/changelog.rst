.. include:: _static/ou_header.rst

.. _changelog:

#########
Changelog
#########
- :release:`0.1.17 <2019.07.17>`
- :bug:`-` Fixed issue where only IC components would have HTML tables generated. Now all XML output file
  have a corresponding HTML table and descriptions, at the level in hierarchy.
- :release:`0.1.16 <2019.03.05>`
- :bug:`-` Fixed paths to files referenced in packaged version.
- :support:`-` Restructured layout of ``xml2vhdl`` to allow for use of ``setuptools``. Added :mod:`setup`
  which can be executed using: ``python setup.py sdist bdist_wheel`` from:
  ``$REPO_ROOT/tools/xml2vhdl/scripts/python/xml2vhdl-uo``
- :support:`-` Added ``MANIFEST.in`` to include all files required by ``xml2vhdl`` to ``PyPi``.
- :support:`-` Added ``./docs/sphinx/source/_static`` contents to ``git`` for Read the Docs.
- :support:`-` Added ``breathe`` to ``requirements.txt`` for Read the Docs.
- :support:`-` Added ``releases`` to ``requirements.txt`` for Read the Docs.
- :support:`-` Added ``LICENSE_STFC``  for referencing license from code developed by STFC.
- :support:`-` Initial documentation of modules by providing headers in each ``*.py`` file.
- :support:`-` Added License link in README.
- :feature:`-` Added ``releases`` support with this changelog file.
- :support:`-` Updating documentation for :mod:`xml2vhdl`
